# BSG #

BSG(Bulk Shellcode Generator) is used to create multiple shellcodes using msfvenom. 

### Requirements ###

* Metasploit
* Linux

### Features ###

* Multiple payload selection
* Multiple encoders 
* Iterations for encoders 
* Platform & arch selection

More info at [http://www.crazynoobz.net/2016/02/bulk-shellcode-generator.html](here)